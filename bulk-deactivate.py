#required modules
import snyk
import datetime
import os
import json

def process_org(snykclient, orgid, bulkaction, sizeofbatch):    
    collection_proj = []
    proj_ids = []
    projects = snykclient.organizations.get(orgid).projects.all()
    counter=0

    #look and get projectid and batch the collection of project ids
    for p in projects:
        #print(f'projectid: {p.id}')
        proj_ids.append(p.id)
        counter=counter+1
        if counter>sizeofbatch:
            collection_proj.append(proj_ids)
            proj_ids=[]
            counter=0

    #for each collection of project ids - update it
    for projects in collection_proj:
        print (f'START{projects}END')
        batch_update(snykclient, orgid, projects, bulkaction)

    return

def batch_update(client, orgid, p_id, bulkaction):
    proj_filter = {'projects':p_id}
    print(f'org/{orgid}/projects/{bulkaction}')
    print(f'{proj_filter}')

    #POST command
    req = client.post(f'org/{orgid}/projects/{bulkaction}', proj_filter)
    results = json.loads(req.text)
    
    #No checking of error
    print(f'Results = {results}')

    return

##########################
# Customize these values #
##########################
SNYK_TOKEN="<insert your token here>"
orgid="<insert org id here>"
sizeofbatch = 10
##########################

bulkaction = "bulk-deactivate"

# this sets the session to include retries in case of api timeouts etc
client = snyk.SnykClient(SNYK_TOKEN, tries=3, delay=1, backoff=1)

process_org(client, orgid, bulkaction, sizeofbatch)