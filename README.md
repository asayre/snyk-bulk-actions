# Snyk Bulk Actions

Steps: (needs python 3)
1. Install pysnyk:

```bash
pip3 install pysnyk
```

2. Open the attached script and change the parameters in the ***** section for your need. - Test it out first.
3. To run the script type: 
```bash
python3 bulk-deactivate.py
```
This will deactivate (or reactivate or delete, depending on how the script is modified) all projects based on a org ID